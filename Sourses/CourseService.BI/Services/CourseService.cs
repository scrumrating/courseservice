﻿using CourseService.BI.ServiceInterfaces;
using CourseService.Data.DTO;
using CourseService.Data.Entities;
using CourseService.Data.Interfaces;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseService.BI.Services
{
    public class CourseService : ICourseService
    {
        private readonly ICourseRepository repo;

        public CourseService(ICourseRepository repo)
        {
            this.repo = repo;
        }
        public bool Create(CourseDto course)
        {
            return this.repo.Create(new Course(course.Id, course.Name, course.StartDate, course.FinishDate, course.Description));
        }

        public bool Delete(int id)
        {
            return repo.Delete(id);
        }

        public CourseDto Get(int id)
        {
            var item = repo.Get(id);
            if (item != null)
            { 
                return new CourseDto(item.Id, item.Name, item.StartDate, item.FinishDate, item.Description);
            }
            return null;
        }

        public List<CourseDto> GetAll(bool onlyActive)
        {
            var list = repo.GetAll(onlyActive);
            return list.Select(x => new CourseDto(x.Id, x.Name, x.StartDate, x.FinishDate, x.Description)).ToList();
        }

        public bool Update(CourseDto course)
        {
            return repo.Update(new Course(course.Id, course.Name, course.StartDate, course.FinishDate, course.Description));
        }
    }
}
