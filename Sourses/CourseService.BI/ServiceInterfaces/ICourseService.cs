﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.BI.ServiceInterfaces
{
    public interface ICourseService
    {
        List<CourseDto> GetAll(bool onlyActive);
        CourseDto Get(int id);
        bool Create(CourseDto course);
        bool Update(CourseDto course);
        bool Delete(int id);
    }
}
