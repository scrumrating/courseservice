﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Data.DTO
{
    /// <summary>
    /// Сущность "Курс".
    /// </summary>
    public class CourseDto
    {
        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CourseDto"/>.
        /// </summary>
        /// <param name="id">Идентификатор</param>
        /// <param name="name">Наименование</param>
        /// <param name="startDate">Дата начала</param>
        /// <param name="finishDate">Дата окончания</param>
        /// <param name="description">Описание</param>
        public CourseDto(int id, string name, DateTime startDate, DateTime finishDate, string description)
        {
            Id = id;
            Name = name;
            StartDate = startDate;
            FinishDate = finishDate;
            Description = description;
        }

        /// <summary>
        /// Идентификатор.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Дата начала 
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Дата окончания 
        /// </summary>
        public DateTime FinishDate { get; set; }

        /// <summary>
        /// Описание
        /// </summary>
        public string Description { get; set; }


    }
}
