﻿using CourseService.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace CourseService.Data
{
    public class CourseDbContext: DbContext
    {
        public CourseDbContext(DbContextOptions<CourseDbContext> options): base (options)
        {            
        }

        public DbSet<Course> Courses { get; set; }

        /*protected override void OnModelCreating(ModelBuilder builder)
        {
            var table = builder.Entity<Course>();
            
            table.Property(x => x.Id).IsRequired();
            table.Property(x => x.Name).IsRequired().HasMaxLength(200);
            table.Property(x => x.StartDate).IsRequired();
            table.Property(x => x.FinishDate).IsRequired();
        } */       
    }
}
