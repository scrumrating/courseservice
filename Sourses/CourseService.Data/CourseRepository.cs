﻿using CourseService.Data.Entities;
using CourseService.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CourseService.Data
{
    /// <summary>
    /// Репозиторий для работы с данными курсов.
    /// </summary>
    public class CourseRepository : ICourseRepository
    {
        private readonly CourseDbContext context;

        public CourseRepository(CourseDbContext context)
        {
            this.context = context;
        }

        public bool Create(Course course)
        {
            context.Courses.Add(course);
            context.SaveChanges();
            return true;
        }

        public bool Delete(int id)
        {
            var student = context.Courses.Find(id);
            if (student != null)
            {                
                context.Courses.Remove(student);
                context.SaveChanges();
                return true;
            }
            return false;

        }

        public Course Get(int id)
        {
            return context.Courses.FirstOrDefault(x=>x.Id == id && x.IsDeleted == false);
        }

        public List<Course> GetAll(bool onlyActive)
        {
            return context.Courses
                .Where(x=> x.IsDeleted == false &&(onlyActive==false||(onlyActive && x.FinishDate> DateTime.Now.Date))).ToList();
        }

        public bool Update(Course course)
        {
            context.Courses.Update(course);
            context.SaveChanges();
            return true;
        }
    }
}
