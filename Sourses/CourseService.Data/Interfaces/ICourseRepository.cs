﻿using CourseService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Data.Interfaces
{
    public interface ICourseRepository
    {
        List<Course> GetAll(bool onlyActive);
        Course Get(int id);
        bool Create(Course course);
        bool Update(Course course);
        bool Delete(int id);
    }
}
