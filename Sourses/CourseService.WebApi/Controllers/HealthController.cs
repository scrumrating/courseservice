﻿using CourseService.Consumers.Healthchecks;
using MassTransit;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CourseService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер проверки состояния службы.
    /// </summary>
    [Route("api/health")]
    public class HealthController : Controller
    {
        private readonly IRequestClient<HealthcheckCommand> healthClient;

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="HealthController"/>.
        /// </summary>
        /// <param name="healthClient">Клиент получения статуса службы.</param>
        public HealthController(IRequestClient<HealthcheckCommand> healthClient)
        {
            this.healthClient = healthClient;
        }

        /// <summary>
        /// Метод для получения доступным методов.
        /// </summary>
        /// <returns>Список доступных методов.</returns>
        [HttpOptions]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        public IActionResult Options()
        {
            this.Response.Headers.Add("Allow", "OPTIONS, GET");
            return this.Ok();
        }

        /// <summary>
        /// Получить статус службы.
        /// </summary>
        /// <returns>Статус службы.</returns>
        [HttpGet]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.GatewayTimeout)]
        [ProducesResponseType((int)HttpStatusCode.BadGateway)]
        public async Task<IActionResult> Get()
        {
            try
            {
                Response<HealthcheckResponse> response = await this.healthClient.GetResponse<HealthcheckResponse>(new HealthcheckCommand());

                return response.Message.Result == "success" ? this.Ok() : new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}
