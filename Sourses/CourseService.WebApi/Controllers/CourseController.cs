﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CourseService.Consumers.Create;
using CourseService.Consumers.Delete;
using CourseService.Consumers.Get;
using CourseService.Consumers.GetAll;
using CourseService.Consumers.Update;
using CourseService.Data.DTO;
using MassTransit;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CourseService.WebApi.Controllers
{
    /// <summary>
    /// Контроллер Course.
    /// </summary>
    [Route("api/course")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        private readonly IRequestClient<GetAllCommand> getAllClient;
        private readonly IRequestClient<GetOneCommand> getOneClient;
        private readonly IRequestClient<CreateCommand> createClient;
        private readonly IRequestClient<UpdateCommand> updateClient;
        private readonly IRequestClient<DeleteCommand> deleteClient;


        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="StudentsController"/>.
        /// </summary>
        /// <param name="getAllClient">Клиент получения все записи</param>
        /// <param name="getOneClient">Клиент одну запись по id</param>
        /// <param name="createClient">Клиент создает 1 запись.</param>
        /// <param name="updateClient">Клиент обновляет 1 запись</param>
        /// <param name="deleteClient">Удаляет одну запись по id</param>
        public CourseController(IRequestClient<GetAllCommand> getAllClient, 
                                IRequestClient<GetOneCommand> getOneClient, 
                                IRequestClient<CreateCommand> createClient, 
                                IRequestClient<UpdateCommand> updateClient, 
                                IRequestClient<DeleteCommand> deleteClient)
        {
            this.getAllClient = getAllClient;
            this.getOneClient = getOneClient;
            this.createClient = createClient;
            this.updateClient = updateClient;
            this.deleteClient = deleteClient;
        }

        /// <summary>
        /// Получить информацию о всех курсах
        /// </summary>
        /// <returns>Список курсов</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var command = new GetAllCommand();
                var response = await this.getAllClient.GetResponse<GetAllResponse>(command);

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Courses);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }


        /// <summary>
        /// Получить курс по id
        /// </summary>
        /// <param name="id">Идентификатор курса</param>
        /// <returns>Курс</returns>
        [HttpGet]
        [Route("GetById")]
        public async Task<IActionResult> GetById([FromQuery(Name = "id")] int id)
        {
            try
            {
                var response = await this.getOneClient.GetResponse<GetOneResponse>(new GetOneCommand { Id=id});

                switch (response.Message.Result)
                {
                    case "success":
                        return this.Ok(response.Message.Course);
                    case "not-found":
                        return new StatusCodeResult((int)HttpStatusCode.NotFound);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Создать курс
        /// </summary>
        /// <param name="course">Данные о курсе</param>
        [HttpPost]
        public async Task<IActionResult> CreateOne([FromBody] CourseDto course)
        {
            try
            {
                Response<CreateResponse> response = await this.createClient.GetResponse<CreateResponse>(new CreateCommand { Course = course });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    case "not-created":
                        return new StatusCodeResult((int)HttpStatusCode.InternalServerError);
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Обновить курс
        /// </summary>
        /// <param name="course">Данные о курсе</param>
        [HttpPatch]
        public async Task<IActionResult> UpdateOne([FromBody] CourseDto course)
        {
            try
            {
                var response = await this.updateClient.GetResponse<UpdateResponse>(new UpdateCommand { Course = course });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception)
            {
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }

        /// <summary>
        /// Удалить курс
        /// </summary>
        /// <param name="id">Идентификатор</param>
        [HttpDelete]
        public async Task<IActionResult> DeleteOne([FromQuery] int id)
        {
            try
            {
                var response = await this.deleteClient.GetResponse<DeleteResponse>(new DeleteCommand { Id = id });

                switch (response.Message.Result)
                {
                    case "success":
                        return Ok();
                    default:
                        return new StatusCodeResult((int)HttpStatusCode.BadGateway);
                }
            }
            catch (RequestTimeoutException)
            {
                return new StatusCodeResult((int)HttpStatusCode.GatewayTimeout);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new StatusCodeResult((int)HttpStatusCode.BadGateway);
            }
        }
    }
}