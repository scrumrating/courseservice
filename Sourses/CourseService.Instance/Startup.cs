using CourseService.BI.ServiceInterfaces;
using CourseService.Consumers.Create;
using CourseService.Consumers.Delete;
using CourseService.Consumers.Get;
using CourseService.Consumers.GetAll;
using CourseService.Consumers.Update;
using CourseService.Data;
using CourseService.Data.Interfaces;
using CourseService.Instance.Infrastructure.MessageBroker;
using CourseService.Instance.Infrastructure.Swagger;
using MassTransit;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.IO;

namespace CourseService.Instance
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            ConfigureDataStorage();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseCors("AllowAll");
            app.UseMvc();
            app.UseSwaggerDocumentation(this.Configuration);
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(o => o.AddPolicy("AllowAll", builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            // Swager
            services.AddSwaggerDocumentation(this.Configuration);

            // DB
            services.AddScoped<DbContext, CourseDbContext>();
            services.AddDbContext<CourseDbContext>(option =>
                option.UseNpgsql(this.Configuration.GetConnectionString("CourseDB")));

            // Services
            services.AddScoped<ICourseService, BI.Services.CourseService>();
            services.AddScoped<ICourseRepository, CourseRepository>();
            services.AddMessageBroker(this.Configuration);

            services.AddMvc();
        }


        public void ConfigureDataStorage()
        {
            var contextOptions = new DbContextOptionsBuilder<CourseDbContext>();
            contextOptions.UseNpgsql(this.Configuration.GetConnectionString("CourseDB"));

            using (var context = new CourseDbContext(contextOptions.Options))
            {
                context.Database.EnsureCreated();
            }
        }
    }
}
