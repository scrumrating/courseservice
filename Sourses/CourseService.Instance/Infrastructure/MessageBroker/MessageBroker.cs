﻿using CourseService.Consumers.Create;
using CourseService.Consumers.Delete;
using CourseService.Consumers.Get;
using CourseService.Consumers.GetAll;
using CourseService.Consumers.Infrastructure;
using CourseService.Consumers.Update;
using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourseService.Instance.Infrastructure.MessageBroker
{
    internal static class MessageBroker
    {
        /// <summary>
        /// Регистрирует броекр сообщений.
        /// </summary>
        /// <param name="services">Список сервисов.</param>
        /// <returns>Изменённый список сервисов.</returns>
        public static IServiceCollection AddMessageBroker(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddConsumers();

            // Регистрация потребителей сообщений
            services.AddMassTransit(x =>
            {
                x.AddConsumer<GetAllConsumer>();
                x.AddConsumer<GetOneConsumer>();
                x.AddConsumer<CreateConsumer>();
                x.AddConsumer<UpdateConsumer>();
                x.AddConsumer<DeleteConsumer>();
            });

            // Регистрация шины.
            // Подробнее про регистрацию шины можно почитать здесь: https://masstransit-project.com/usage/
            services.AddSingleton(serviceProvider => Bus.Factory.CreateUsingRabbitMq(configure =>
            {
                BusConfiguration busConfiguration = configuration.GetSection("Bus").Get<BusConfiguration>();

                // Конфигурация подключения к шине, включающая в себя указание адреса и учетных данных.
                configure.Host(new Uri(busConfiguration.ConnectionString), host =>
                            {
                                host.Username(busConfiguration.Username);
                                host.Password(busConfiguration.Password);

                    // Подтверждение получения гарантирует доставку сообщений за счет ухудшения производительности.
                    host.PublisherConfirmation = busConfiguration.PublisherConfirmation;
                            });

                // Добавление Serilog для журналирования внутренностей MassTransit.
                configure.UseSerilog();

                // Регистрация очередей и их связи с потребителями сообщений.
                // В качестве метки сообщения используется полное имя класса сообщения, которое потребляет потребитель.



                configure.ReceiveEndpoint(typeof(GetAllCommand).FullName, endpoint => { endpoint.Consumer<GetAllConsumer>(serviceProvider); EndpointConvention.Map<GetAllCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(GetOneCommand).FullName, endpoint => { endpoint.Consumer<GetOneConsumer>(serviceProvider); EndpointConvention.Map<GetOneCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(CreateCommand).FullName, endpoint => { endpoint.Consumer<CreateConsumer>(serviceProvider); EndpointConvention.Map<CreateCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(UpdateCommand).FullName, endpoint => { endpoint.Consumer<UpdateConsumer>(serviceProvider); EndpointConvention.Map<UpdateCommand>(endpoint.InputAddress); });
                configure.ReceiveEndpoint(typeof(DeleteCommand).FullName, endpoint => { endpoint.Consumer<DeleteConsumer>(serviceProvider); EndpointConvention.Map<DeleteCommand>(endpoint.InputAddress); });

            }));

            // Регистрация сервисов MassTransit.
            services.AddSingleton<IPublishEndpoint>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<ISendEndpointProvider>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());
            services.AddSingleton<IBus>(serviceProvider => serviceProvider.GetRequiredService<IBusControl>());

            // Регистрация клиентов для запроса данных от потребителей сообщений из api.
            // Каждый клиент зарегистрирован таким образом, что бы в рамках каждого запроса к api существовал свой клиент.
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetAllCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<GetOneCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<CreateCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<UpdateCommand>());
            services.AddScoped(serviceProvider => serviceProvider.GetRequiredService<IBus>().CreateRequestClient<DeleteCommand>());


            // Регистрация сервиса шины MassTransit.
            services.AddSingleton<IHostedService, BusService>();
            return services;
        }
    }
}
