﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Create
{
    public class CreateResponse
    {
        public string Result { get; set; }

        public CreateResponse(string result)
        {
            Result = result;
        }

        public CreateResponse()
        {
        }
    }
}
