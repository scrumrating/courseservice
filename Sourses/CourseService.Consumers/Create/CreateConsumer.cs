﻿using CourseService.BI.ServiceInterfaces;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CourseService.Consumers.Create
{
    public class CreateConsumer : IConsumer<CreateCommand>
    {
        private readonly ICourseService service;

        public CreateConsumer(ICourseService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<CreateCommand> context)
        {
            var item = service.Create(context.Message.Course);
            if (item)
                await context.RespondAsync(new CreateResponse(ConsumerConsts.success));
            else
                await context.RespondAsync(new CreateResponse(ConsumerConsts.notfcreated));
        }
    }
}
