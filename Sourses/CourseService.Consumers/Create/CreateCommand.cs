﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Create
{
    public class CreateCommand
    {
        public CourseDto Course { get; set; }
    }
}
