﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Delete
{
    public class DeleteResponse
    {
        public string Result { get; set; }

        public DeleteResponse(string result)
        {
            Result = result;
        }

        public DeleteResponse()
        {
        }
    }
}
