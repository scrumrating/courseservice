﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Delete
{
    public class DeleteCommand
    {
        public int Id { get; set; }
    }
}
