﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Update
{
    public class UpdateCommand
    {
        public CourseDto Course { get; set; }
    }
}
