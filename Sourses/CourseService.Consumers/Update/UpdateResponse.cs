﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Update
{
    public class UpdateResponse
    {
        public CourseDto Course { get; set; }
        public string Result { get; set; }

        public UpdateResponse(CourseDto course, string result)
        {
            Course = course;
            Result = result;
        }

        public UpdateResponse(string result)
        {
            Result = result;
        }

        public UpdateResponse()
        {
        }
    }
}
