﻿using CourseService.BI.ServiceInterfaces;
using MassTransit;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CourseService.Consumers.Update
{
    public class UpdateConsumer : IConsumer<UpdateCommand>
    {
        private readonly ICourseService service;

        public UpdateConsumer(ICourseService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<UpdateCommand> context)
        {
            var item = service.Update(context.Message.Course);
            if (item)
                await context.RespondAsync(new UpdateResponse(ConsumerConsts.success));
            else
                await context.RespondAsync(new UpdateResponse(ConsumerConsts.notfound));
        }
    }
}
