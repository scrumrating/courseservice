﻿using CourseService.BI.ServiceInterfaces;
using MassTransit;
using System.Threading.Tasks;

namespace CourseService.Consumers.GetAll
{

    public class GetAllConsumer: IConsumer<GetAllCommand>
    {
        private readonly ICourseService service;


        public GetAllConsumer(ICourseService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<GetAllCommand> context)
        {
            var list = service.GetAll(context.Message.OnlyActive);
            if (list != null)
                await context.RespondAsync(new GetAllResponse(list, ConsumerConsts.success));
            else
                await context.RespondAsync(new GetAllResponse(ConsumerConsts.notfound));
        }
    }
}
