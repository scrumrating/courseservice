﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.GetAll
{
    public class GetAllCommand
    {
        public bool OnlyActive { get; set; } = false;
    }
}
