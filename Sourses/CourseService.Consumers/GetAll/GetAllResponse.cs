﻿using CourseService.Data.DTO;
using CourseService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.GetAll
{
    public class GetAllResponse
    {
        public List<CourseDto> Courses { get; set; }
        public string Result { get; set; }

        public GetAllResponse(List<CourseDto> courses, string result)
        {
            Courses = courses;
            Result = result;
        }

        public GetAllResponse(string result)
        {
            Result = result;
        }

        public GetAllResponse()
        {
        }
    }
}
