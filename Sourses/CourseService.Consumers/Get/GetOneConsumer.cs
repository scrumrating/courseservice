﻿using CourseService.BI.ServiceInterfaces;
using MassTransit;
using System.Threading.Tasks;

namespace CourseService.Consumers.Get
{
    public class GetOneConsumer : IConsumer<GetOneCommand>
    {
        private readonly ICourseService service;
        public GetOneConsumer(ICourseService service)
        {
            this.service = service;
        }

        public async Task Consume(ConsumeContext<GetOneCommand> context)
        {
            var item = service.Get(context.Message.Id);
            if (item != null)
                await context.RespondAsync(new GetOneResponse(item, ConsumerConsts.success));
            else
                await context.RespondAsync(new GetOneResponse(ConsumerConsts.notfound));
        }
    }
}
