﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Get
{
    public class GetOneCommand
    {
        public int Id { get; set; }
    }
}
