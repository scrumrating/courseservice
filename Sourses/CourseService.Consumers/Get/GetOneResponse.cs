﻿using CourseService.Data.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace CourseService.Consumers.Get
{
    public class GetOneResponse
    {
        public CourseDto Course { get; set; }
        public string Result { get; set; }

        public GetOneResponse(CourseDto course, string result)
        {
            Course = course;
            Result = result;
        }

        public GetOneResponse(string result)
        {
            Result = result;
        }

        public GetOneResponse()
        {
        }
    }
}
